// MASK PLUGIN
!function(i){"use strict";var l=function(v,b,w){var C={invalid:[],getCaret:function(){try{var t,a=0,e=v.get(0),n=document.selection,s=e.selectionStart;return n&&-1===navigator.appVersion.indexOf("MSIE 10")?((t=n.createRange()).moveStart("character",-C.val().length),a=t.text.length):(s||"0"===s)&&(a=s),a}catch(t){}},setCaret:function(t){try{if(v.is(":focus")){var a,e=v.get(0);e.setSelectionRange?e.setSelectionRange(t,t):((a=e.createTextRange()).collapse(!0),a.moveEnd("character",t),a.moveStart("character",t),a.select())}}catch(t){}},events:function(){v.on("keydown.mask",function(t){v.data("mask-keycode",t.keyCode||t.which),v.data("mask-previus-value",v.val()),v.data("mask-previus-caret-pos",C.getCaret()),C.maskDigitPosMapOld=C.maskDigitPosMap}).on(i.jMaskGlobals.useInput?"input.mask":"keyup.mask",C.behaviour).on("paste.mask drop.mask",function(){setTimeout(function(){v.keydown().keyup()},100)}).on("change.mask",function(){v.data("changed",!0)}).on("blur.mask",function(){o===C.val()||v.data("changed")||v.trigger("change"),v.data("changed",!1)}).on("blur.mask",function(){o=C.val()}).on("focus.mask",function(t){!0===w.selectOnFocus&&i(t.target).select()}).on("focusout.mask",function(){w.clearIfNotMatch&&!r.test(C.val())&&C.val("")})},getRegexMask:function(){for(var t,a,e,n,s,r,o=[],i=0;i<b.length;i++)(t=P.translation[b.charAt(i)])?(a=t.pattern.toString().replace(/.{1}$|^.{1}/g,""),e=t.optional,(n=t.recursive)?(o.push(b.charAt(i)),s={digit:b.charAt(i),pattern:a}):o.push(e||n?a+"?":a)):o.push(b.charAt(i).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&"));return r=o.join(""),s&&(r=r.replace(new RegExp("("+s.digit+"(.*"+s.digit+")?)"),"($1)?").replace(new RegExp(s.digit,"g"),s.pattern)),new RegExp(r)},destroyEvents:function(){v.off(["input","keydown","keyup","paste","drop","blur","focusout",""].join(".mask "))},val:function(t){var a,e=v.is("input")?"val":"text";return 0<arguments.length?(v[e]()!==t&&v[e](t),a=v):a=v[e](),a},calculateCaretPosition:function(){var t=v.data("mask-previus-value")||"",a=C.getMasked(),e=C.getCaret();if(t!==a){var n=v.data("mask-previus-caret-pos")||0,s=a.length,r=t.length,o=0,i=0,l=0,c=0,u=0;for(u=e;u<s&&C.maskDigitPosMap[u];u++)i++;for(u=e-1;0<=u&&C.maskDigitPosMap[u];u--)o++;for(u=e-1;0<=u;u--)C.maskDigitPosMap[u]&&l++;for(u=n-1;0<=u;u--)C.maskDigitPosMapOld[u]&&c++;if(r<e)e=10*s;else if(e<=n&&n!==r){if(!C.maskDigitPosMapOld[e]){var k=e;e-=c-l,e-=o,C.maskDigitPosMap[e]&&(e=k)}}else n<e&&(e+=l-c,e+=i)}return e},behaviour:function(t){t=t||window.event,C.invalid=[];var a=v.data("mask-keycode");if(-1===i.inArray(a,P.byPassKeys)){var e=C.getMasked(),n=C.getCaret();return setTimeout(function(){C.setCaret(C.calculateCaretPosition())},i.jMaskGlobals.keyStrokeCompensation),C.val(e),C.setCaret(n),C.callbacks(t)}},getMasked:function(t,a){var e,n,s,r=[],o=void 0===a?C.val():a+"",i=0,l=b.length,c=0,u=o.length,k=1,v="push",h=-1,p=0,f=[];for(w.reverse?(v="unshift",k=-1,e=0,i=l-1,c=u-1,n=function(){return-1<i&&-1<c}):(e=l-1,n=function(){return i<l&&c<u});n();){var d=b.charAt(i),g=o.charAt(c),m=P.translation[d];m?(g.match(m.pattern)?(r[v](g),m.recursive&&(-1===h?h=i:i===e&&i!==h&&(i=h-k),e===h&&(i-=k)),i+=k):g===s?(p--,s=void 0):m.optional?(i+=k,c-=k):m.fallback?(r[v](m.fallback),i+=k,c-=k):C.invalid.push({p:c,v:g,e:m.pattern}),c+=k):(t||r[v](d),g===d?(f.push(c),c+=k):(s=d,f.push(c+p),p++),i+=k)}var M=b.charAt(e);l!==u+1||P.translation[M]||r.push(M);var y=r.join("");return C.mapMaskdigitPositions(y,f,u),y},mapMaskdigitPositions:function(t,a,e){var n=w.reverse?t.length-e:0;C.maskDigitPosMap={};for(var s=0;s<a.length;s++)C.maskDigitPosMap[a[s]+n]=1},callbacks:function(t){var a=C.val(),e=a!==o,n=[a,t,v,w],s=function(t,a,e){"function"==typeof w[t]&&a&&w[t].apply(this,e)};s("onChange",!0===e,n),s("onKeyPress",!0===e,n),s("onComplete",a.length===b.length,n),s("onInvalid",0<C.invalid.length,[a,t,v,C.invalid,w])}};v=i(v);var r,P=this,o=C.val();b="function"==typeof b?b(C.val(),void 0,v,w):b,P.mask=b,P.options=w,P.remove=function(){var t=C.getCaret();return P.options.placeholder&&v.removeAttr("placeholder"),v.data("mask-maxlength")&&v.removeAttr("maxlength"),C.destroyEvents(),C.val(P.getCleanVal()),C.setCaret(t),v},P.getCleanVal=function(){return C.getMasked(!0)},P.getMaskedVal=function(t){return C.getMasked(!1,t)},P.init=function(t){if(t=t||!1,w=w||{},P.clearIfNotMatch=i.jMaskGlobals.clearIfNotMatch,P.byPassKeys=i.jMaskGlobals.byPassKeys,P.translation=i.extend({},i.jMaskGlobals.translation,w.translation),P=i.extend(!0,{},P,w),r=C.getRegexMask(),t)C.events(),C.val(C.getMasked());else{w.placeholder&&v.attr("placeholder",w.placeholder),v.data("mask")&&v.attr("autocomplete","off");for(var a=0,e=!0;a<b.length;a++){var n=P.translation[b.charAt(a)];if(n&&n.recursive){e=!1;break}}e&&v.attr("maxlength",b.length).data("mask-maxlength",!0),C.destroyEvents(),C.events();var s=C.getCaret();C.val(C.getMasked()),C.setCaret(s)}},P.init(!v.is("input"))};i.maskWatchers={};var a=function(){var t=i(this),a={},e="data-mask-",n=t.attr("data-mask");if(t.attr(e+"reverse")&&(a.reverse=!0),t.attr(e+"clearifnotmatch")&&(a.clearIfNotMatch=!0),"true"===t.attr(e+"selectonfocus")&&(a.selectOnFocus=!0),c(t,n,a))return t.data("mask",new l(this,n,a))},c=function(t,a,e){e=e||{};var n=i(t).data("mask"),s=JSON.stringify,r=i(t).val()||i(t).text();try{return"function"==typeof a&&(a=a(r)),"object"!=typeof n||s(n.options)!==s(e)||n.mask!==a}catch(t){}};i.fn.mask=function(t,a){a=a||{};var e=this.selector,n=i.jMaskGlobals,s=n.watchInterval,r=a.watchInputs||n.watchInputs,o=function(){if(c(this,t,a))return i(this).data("mask",new l(this,t,a))};return i(this).each(o),e&&""!==e&&r&&(clearInterval(i.maskWatchers[e]),i.maskWatchers[e]=setInterval(function(){i(document).find(e).each(o)},s)),this},i.fn.masked=function(t){return this.data("mask").getMaskedVal(t)},i.fn.unmask=function(){return clearInterval(i.maskWatchers[this.selector]),delete i.maskWatchers[this.selector],this.each(function(){var t=i(this).data("mask");t&&t.remove().removeData("mask")})},i.fn.cleanVal=function(){return this.data("mask").getCleanVal()},i.applyDataMask=function(t){((t=t||i.jMaskGlobals.maskElements)instanceof i?t:i(t)).filter(i.jMaskGlobals.dataMaskAttr).each(a)};var t,e,n,s={maskElements:"input,td,span,div",dataMaskAttr:"*[data-mask]",dataMask:!0,watchInterval:300,watchInputs:!0,keyStrokeCompensation:10,useInput:!/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent)&&(t="input",n=document.createElement("div"),(e=(t="on"+t)in n)||(n.setAttribute(t,"return;"),e="function"==typeof n[t]),n=null,e),watchDataMask:!1,byPassKeys:[9,16,17,18,36,37,38,39,40,91],translation:{0:{pattern:/\d/},9:{pattern:/\d/,optional:!0},"#":{pattern:/\d/,recursive:!0},A:{pattern:/[a-zA-Z0-9]/},S:{pattern:/[a-zA-Z]/}}};i.jMaskGlobals=i.jMaskGlobals||{},(s=i.jMaskGlobals=i.extend(!0,{},s,i.jMaskGlobals)).dataMask&&i.applyDataMask(),setInterval(function(){i.jMaskGlobals.watchDataMask&&i.applyDataMask()},s.watchInterval)}(jQuery);

$(document).ready(function () {
    var $boxClientInfo = $('.box-client-info-pf');
    var url = 'https://mercostore.vtexcommercestable.com.br/api/dataentities/CL/documents/{{id}}';
    var CL_ID = null;

    function isValidDate(date) {
        var temp = date.split('/');
        var d = new Date(temp[1] + '/' + temp[0] + '/' + temp[2]);
         return (d && (d.getMonth() + 1) == temp[1] && d.getDate() == Number(temp[0]) && d.getFullYear() == Number(temp[2]));
    }

    if ($boxClientInfo.length && !$boxClientInfo.find('.admake-birth-date').length) {
        var $birthDate = $('<div class="admake-birth-date">' +
            '   <p class="client-birth-date input pull-left text">' +
            '       <label for="client-birth-date">Data de nascimento</label>' +
            '       <input type="text" id="client-birth-date" class="input-small mask-date" maxlength="10" />' +
            '   </p>' +
            '</div>');
        $boxClientInfo.append($birthDate);

        $('.mask-date').mask('00/00/0000');

        $(document).on('blur', '#client-birth-date', function(){
            var date = $(this).val();

            if( !isValidDate(date) ){
                $(this).val('');
                alert('Data inválida');
                return false;
            }
            date = date.split('/');

            var data = {
                birthDate: date[2] + '-' + date[1] + '-' + date[0]
            }
            if( CL_ID == null ){
                return false;
            }
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://mercostore.vtexcommercestable.com.br/api/dataentities/CL/documents/" + CL_ID,
                "method": "PATCH",
                "type": "PATCH",
                "headers": {
                  "Content-Type": "application/json",
                  "Accept": "application/vnd.vtex.ds.v10+json",
                },
                "processData": false,
                "data": JSON.stringify(data)
              }
              
              $.ajax(settings).done(function (response) {
                console.log(response);
              });
        });

        vtexjs.checkout.getOrderForm().done(function(orderForm) {
            console.log(orderForm);
            var email = orderForm['clientProfileData']['email'] || null;

            if( email !== undefined && email !== null ){
                var settings = {
                    "async": true,
                    "url": "https://mercostore.vtexcommercestable.com.br/api/dataentities/CL/search?_fields=id,email,birthDate&email=" + email,
                    "method": "GET",
                }
        
                $.ajax(settings).done(function (response) {
                    if( response != undefined && response != null && response.length ){
                        console.log(response)
                        var id = response[0]['id'];
                        var data = response[0]['birthDate'];
                        if( id != undefined && id != null ){
                            CL_ID = id;
                            $('#client-birth-date').attr('data-id', id);
                            if( data != undefined && data != null ){
                                data = new Date(data);
                                var dia = data.getDate();
                                dia = ( dia.toString().length == 1 ) ? '0' + dia.toString() : dia;
                                var mes = (data.getMonth() + 1);
                                mes = ( mes.toString().length == 1 ) ? '0' + mes.toString() : mes;
                                var ano = data.getFullYear();
                                $('#client-birth-date').val(dia + '/' + mes + '/' + ano);
                            }
                        }
                    }
                });
            }

            
        });
    }
});

/* <script src="https://www.googletagmanager.com/gtag/js?id=UA-122339413-1"></script> */
window.dataLayer = window.dataLayer || [];

function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());

gtag('config', 'UA-122339413-1');