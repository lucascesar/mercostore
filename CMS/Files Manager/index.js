/*************** CSS ***************/
import './css/sass/admake-default.scss'

/*************** JS ***************/

// PLUGINS
import './js/plugins/slick.js'
import './js/plugins/jquery.mask.js'
import './js/plugins/counterdown.js'

// FUCTIONS
import { observerMutation, reset, accordion } from './js/components/_functions.js'

// COMPONENTS
import carroselDefault from './js/components/_carrosel.js'
import Cart from './js/components/_cart.js'
import navbar from './js/components/_navbar.js'
import Newsletter from './js/components/_newsletter.js'
import counterDown from './js/components/_counterDown.js'
import showcase from './js/components/_showcase'
import addRemQty from './js/components/_addRemQty.js'

// PAGES
import Institucional from './js/pages/_institucional.js'
import Department from './js/pages/_department.js'
import Product from './js/pages/_product.js'
import Account from './js/pages/_account.js'

setTimeout(function () {
    $('a[href="/no-cache/user/logout"]').addClass('logout').before('<a href="/_secure/account" class="account">Minha conta</a><i class="m-1">|</i>');
}, 1000);

$(document).ready(function () {
    reset();
    carroselDefault();
    counterDown();
    accordion();
    navbar();
    showcase();
    addRemQty();

    observerMutation('.ui-autocomplete', function () {
        $('.ui-autocomplete li img').each(function () {
            var $this = $(this), href = $this.attr('src').replace('25-25', '90-90');
            $this.attr('src', href);
        });
    });

    $('.benefits-banner ul').slick({
        dots: false,
        arrows: false,
        infinite: true,
        slidesToShow: 5,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    variableWidth: false
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    variableWidth: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    variableWidth: false
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    variableWidth: false
                }
            },
        ]
    });


    // /** PAGES */
    Institucional();
    Department();
    Product();
    Account();
    // /** PAGES */

    // new Navbar;
    new Cart;
    new Newsletter;

})



