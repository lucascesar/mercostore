﻿export default function addRemQty(elementQty = '.btn-add-remove') {

    // EVENT QTY
    $(document).on('click', elementQty, function () {
        const $this = $(this);
        let type = $this.attr('type') || '',
            $parent = $this.parents(($this.attr('parent') || '')), value = 0, $e = null;

        if ($parent.length && type !== '') {
            $e = $parent.find(($this.attr('element') || ''));

            if ($e.length) {
                value = parseInt(($e.val() || 1));

                if (type == 'add') {
                    $e.val(++value);
                } else {
                    if(value > 1){
                        $e.val(--value);
                    }
                }
                $e.change();
            }
        }
    });
}
