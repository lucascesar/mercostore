﻿import { stringToSlug } from './_functions.js'

export default function colorPick(element = '[data-admake="color-pick"]') {
    let _$el = $(element), topic, $e = null;

    if (_$el.length && _$el.attr('data-sku')) {
        topic = _$el.attr('data-sku');
        topic = topic.split('|');

        
        vtexjs.catalog.getCurrentProductWithVariations().done(function (product) {
            if (topic.length) {
                for (var x = 0; x < topic.length; x++) {
                    $e = _$el.find('.topic.' + topic[x]);
                    if ($e.length) {
                        $.each($e.find('.dimension-' + topic[x]), function () {
                            let $this = $(this), value = stringToSlug(($this.text().toLowerCase())),
                                url = '/arquivos/color-' + value + '.png';

                            var img = new Image();
                            img.onload = function () {
                                $this.attr('title', $this.text()).html('<img src="' + url + '" alt="' + value + '">').addClass('thumb-icon');
                            };
                            img.onerror = function () {
                                console.log('Not image');
                            };
                            img.src = url;
                        });
                    }
                }
            }
        });
    }
}
