﻿export default function counterDown(element = '[data-admake="counter-down"]', userTime = '.user-timer', timer = '.timer') {
    let _$el = $(element);


    if (_$el.length && _$el.find(userTime).length && $.fn.countdown) {
        _$el.each(function () {
            let $this = $(this),
                user = $this.find(userTime).text(),
                currentDate = new Date(),
                finalDate = new Date(user);

            if (finalDate.getTime() >= currentDate.getTime()) {
                let $timer = $this.find(timer);

                $this.show();
                $this.find('.slick-initialized').slick("refresh");

                $timer.countdown(user, function (event) {
                    if ($this.attr('data-days') == 'true') {
                        $(this).html(
                            event.strftime(
                                `<ul>
                                <li class="days">
                                    %D
                                    <small>dias</small>
                                </li>
                                <li>
                                    %H
                                    <small>horas</small>
                                </li>
                                <li class="colon">:</li>
                                <li>
                                    %M
                                    <small>min</small>
                                </li>
                                <li class="colon">:</li>
                                <li>%S
                                    <small>seg</small>
                                </li>
                            </ul>`
                            )
                        );
                    }else{
                        var totalHours = event.offset.totalDays * 24 + event.offset.hours;
                        $(this).html(
                            event.strftime(
                                `<ul>
                                    <li>
                                        <span>${totalHours}</span>
                                        <small>horas</small>
                                    </li>
                                    <li class="colon">:</li>
                                    <li>
                                        <span>%M</span>
                                        <small>min</small>
                                    </li>
                                    <li class="colon">:</li>
                                    <li>
                                        <span>%S</span>
                                        <small>seg</small>
                                    </li>
                                </ul>`
                            )
                        );
                    }
                }).on("finish.countdown", function () {
                    $this.hide().remove();
                });

            } else {
                $this.remove();
            }
        });
        // $.fn.countdown
    }
}
