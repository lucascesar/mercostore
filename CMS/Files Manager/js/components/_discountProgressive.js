﻿
import getVariables from './_variables.js'
import { getParameter } from './_functions.js'

const _variables = getVariables();

export default class DiscountProgressive {
    constructor(element = '[data-admake="discount-progressive"]') {
        this._element = element;
        this._$el = $(element);
        this._config = null;
        this._obj = {};

        if (this._$el.length) {
            let dataConfig = this._$el.attr('data-config');

            if (dataConfig !== undefined && dataConfig !== null && dataConfig !== '') {
                dataConfig = dataConfig.replace(/\'/g, '"');
                this._config = { ..._variables.discountProgressive, ...JSON.parse(dataConfig) };
            } else {
                this._config = { ..._variables.discountProgressive };
            }

            this.init();
            this.events();
        }
    }
    createStruct() {
        for (const key in this._obj) {
            if (this._obj.hasOwnProperty(key)) {
                const element = this._obj[key], $currentElement = $('[data-sku="' + key + '"]' + this._element);

                if ($currentElement.length) {
                    let html = '', htmlAux;

                    if ($currentElement.parents('.row-product').length) {
                        let qty = '', percent = '', btn = '', skuCurrent = $('.buy-button.buy-button-ref').attr('href');

                        element.forEach((data) => {
                            qty += `<td>${data.quantity}</td>`;
                            percent += `<td>${data.percent}%</td>`;
                            btn += `<td><a href="${skuCurrent.replace(`qty=${getParameter('qty', skuCurrent)}`, `qty=${data.quantity}`)}" class="btn btn-discount-progressive"><i class="fas fa-cart-plus"></i></a></td>`;
                        })

                        html = `<div class="content">
                                    <table class="table">
                                    <tbody>
                                        <tr>
                                            <td class="title">${this._config.textQty}</td>
                                            ${qty}
                                        </tr>
                                        <tr>
                                            <td class="title">${this._config.textDiscount}</td>
                                            ${percent}
                                        </tr>
                                        <tr>
                                            <td></td>
                                            ${btn}
                                        </tr>
                                    </tbody>`;

                    } else {
                        html = `<div class="content"><table class="table"><thead><tr><th>${this._config.textQty}</th><th>${this._config.textDiscount}</th><th></th></tr></thead><tbody>`;
                        htmlAux = element.map((data) => {
                            return (
                                `<tr>
                                    <td>${data.quantity}</td>
                                    <td>${data.percent}%</td>
                                </tr>`
                            )
                        });
                        html += htmlAux.join('') + `</tbody></table></div>`;
                    }
                    $currentElement.append(html);
                }
            }
        }
    }
    getObjectDiscount() {
        $.ajax({
            "url": `/api/rnb/pvt/calculatorconfiguration/${this._config.idCalculator}/`,
            "method": "GET",
            "headers": {
                "Content-Type": "application/json",
            },
            success: (data) => {
                data.listSku1BuyTogether.forEach(element => {
                    this._obj[element.id] = data.percentualDiscountValueList;
                });

                if (!$.isEmptyObject(this._obj)) {
                    this.createStruct();
                } else {
                    console.log('discount does not exist');
                }
            },
            error: function (data, status) {
                console.log('errors' + status);
            }
        });
    }
    init() {
        $('.ad-showcase-product').each(function (){
            let $this = $(this),
                sku   = $this.find('.product-insertsku .first input[type="checkbox"]').attr('rel');
                
            if(sku){
                $this.find('.product-discount-progressive').attr('data-sku', sku);
            }
        });

        if ($('#page-product').length) {
            let skuCurrent;

            $('.skuList').each(function () {
                $(this).find('> span > label').eq(0).click();
            });

            skuCurrent = $('.buy-button.buy-button-ref').attr('href');
            if (skuCurrent.indexOf('cart') >= 0) {
                $('.row-product ' + this._element).attr('data-sku', getParameter('sku', skuCurrent));
            }
        }

        this.getObjectDiscount();
    }
    events() {
        $(document).on('change', '.skuselector-specification-label', (e) => {
            let skuCurrent = getParameter('sku', $('.buy-button.buy-button-ref').attr('href'));

            this._$el.empty();
            skuCurrent = $('.buy-button.buy-button-ref').attr('href');

            if (skuCurrent.indexOf('cart') >= 0) {
                $('.row-product ' + this._element).attr('data-sku', getParameter('sku', skuCurrent));
                this.getObjectDiscount();
            }
        });
    }
}

