﻿import getVariables from './_variables.js'
const _variables = getVariables();

export default function estimateDate(sku = '', element = '[data-admake="estimate-date"]') {
    let _$el = $(element), _config = null;

    if (_$el.length) {
        let dataConfig = _$el.attr('data-config');

        if (dataConfig !== undefined && dataConfig !== null && dataConfig !== '') {
            dataConfig = dataConfig.replace(/\'/g, '"');
            _config = { ..._variables.estimateDate, ...JSON.parse(dataConfig) };
        } else {
            _config = { ..._variables.estimateDate };
        }

        if (sku != '') {
            $.ajax({
                "url": `/api/catalog_system/pvt/sku/stockkeepingunitbyid/${sku}`,
                "method": "GET",
                "headers": {
                    "Content-Type": "application/json",
                },
                success: (data) => {
                    if (data.EstimatedDateArrival) {
                        let currentDate = new Date(), estimate = new Date(data.EstimatedDateArrival);
        
                        if (estimate.getTime() >= currentDate.getTime()) {
                            _$el.append(
                                `<div class="content">
                                    <strong>${_config.title}</strong>
                                    <p>${_config.text}</p>
                                </div>`
                            );
                        }
                    }
                },
                error: function (data, status) {
                    console.log('errors' + status);
                }
            });
        } else {
            return false;
        }
    }
}





