﻿export default function navbar() {
	var $menu = $('#header-menu > .nav-items'), $menuMobile = $('#header-nav-mobile .nav-items'), width = $('.container').width() - 255;
	
	$('#menu-vtex h3').each(function (index) {
		let $this = $(this), html = '', htmlMobile = '';
		if ($this.find('+ ul > li').length) {
			if ($('.navbar-banners[data-index="' + index + '"] img').length) {
				
				let srcBanner = $('.navbar-banners[data-index="' + index + '"] img').attr('src');

				html = `<li class="lvl-1-item has-children">
								<a href="${$this.find('a').attr('href')}" class="link-lvl-1">${$this.find('a').text()}</a>
								<div class="nav-sub lvl-2 with-banner justify-content-between" style="width: ${width}px;">
									<ul class="list">
										${$this.find('+ ul').html()}
									</ul>
									<div class="banner">
										<img src="${srcBanner}" alt="banner da categoria ${$this.find('a').text()}"/>
									</div>
								</div>
							</li>`;
			} else {
				html = `<li class="lvl-1-item has-children">
								<a href="${$this.find('a').attr('href')}" class="link-lvl-1">${$this.find('a').text()}</a>
								<div class="nav-sub lvl-2" style="width: ${width}px;">
									<ul class="list">
										${$this.find('+ ul').html()}
									</ul>
								</div>
							</li>`;
			}

			if( $(this).find('a').length ){
				htmlMobile = `<li class="lvl-1-item py-3 has-children">
								<a href="${$this.find('a').attr('href')}" class="link-lvl-1">${$this.find('a').text()}</a>
								<div class="nav-sub lvl-2">
									<div class="sub-header text-center">
										<button class="btn-back btn">
											<i class="fa fa-arrow-left" aria-hidden="true"></i>
										</button>
										<a href="${$this.find('a').attr('href')}" class="link-lvl-1">${$this.find('a').text()}</a>
									</div>
									<ul class="sub-categories">
										${$this.find('+ ul').html()}
									</ul>
								</div>
							</li>`;
			}else{
				htmlMobile = `<li class="lvl-1-item py-3 has-children">
								<a href="#" class="link-lvl-1">${$this.find('.menu-item-texto').text()}</a>
								<div class="nav-sub lvl-2">
									<div class="sub-header text-center">
										<button class="btn-back btn">
											<i class="fa fa-arrow-left" aria-hidden="true"></i>
										</button>
										<a href="#" class="link-lvl-1">${$this.find('.menu-item-texto').text()}</a>
									</div>
									<ul class="sub-categories">
										${$this.find('+ ul').html()}
									</ul>
								</div>
							</li>`;
			}


		} else {
			html = `<li class="lvl-1-item"><a href="${$this.find('a').attr('href')}" class="link-lvl-1">${$this.find('a').text()}</a></li>`;
			htmlMobile = `<li class="lvl-1-item py-3"><a href="${$this.find('a').attr('href')}" class="link-lvl-1">${$this.find('a').text()}</a></li>`;
		}
		$menuMobile.append(htmlMobile);
		$menu.append(html);
	});

	$(window).resize(() => {
		width = $('.container').width() - 254;
		$('#header-menu .nav-sub.lvl-2').css("width", width);
	});
	$(document).on('click', '.nav-mobile-close', function () {
		$(`#header-nav-mobile > .content`).animate({
			left: '-100%'
		}, 300, function () {
			$(`#header-nav-mobile`).fadeOut(100);
		});
	});
	$(document).on('click', '#header-bar-menu button', function () {
		$(`#header-nav-mobile`).fadeIn(100, function () {
			$(`#header-nav-mobile > .content`).animate({
				left: 0
			}, 300);
		});
	});
	$(document).on('click', '#header-nav-mobile .lvl-1-item.has-children .link-lvl-1', function (event) {
        event.preventDefault();
        $('#header-nav-mobile .lvl-1-item > .nav-sub').removeClass('active');
        $(this).parent('.lvl-1-item').find('> .nav-sub').addClass('active');
    });
    $(document).on('click', '#header-nav-mobile .btn-back', function () {
        $(this).parents('.nav-sub').removeClass('active')
	});
	
	if($("#menu-vtex .menu-departamento h3").length){
		
		var offers_week_link = "";
		if($("a.offers-week-link").length){
			offers_week_link = $("a.offers-week-link").attr("href");
		};

		$("#menu-vtex .menu-departamento").append("<div class='box-item-menu box-item-menu-offer-week'>"+
											"<h3 class='offers-week'><span></span><a class='menu-item-texto' href='"+offers_week_link+"'>Ofertas da Semana</a></h3>"+
										"</div>");

		$("#menu-vtex .menu-departamento").prepend("<div class='box-item-menu box-item-menu-all-departments'>"+
			"<h3 class='all-departments'><span></span><a class='menu-item-texto' href=''>Todos os Departamentos</a></h3>"+
		"</div>");

		if($("nav#header-navbar ul.nav-items").length){
			$("nav#header-navbar ul.nav-items").appendTo(".box-item-menu-all-departments");
		};

		$("#menu-vtex .menu-departamento h3").each(function(index){
			
			if(!$(this).parent(".brandFilter").length){
				if($(this).parent(".box-item-menu-offer-week").length){
					$("#menu-vtex .menu-departamento").append("<div class='box-item-menu box-item-menu-offer-week box-item-menu-"+index+"'> <div class='box-submenu'> <div class='box-submenu-1'></div> <div class='box-submenu-2'></div> </div> </div>");
				}else if($(this).parent(".box-item-menu-all-departments").length){
					$("#menu-vtex .menu-departamento").append("<div class='box-item-menu box-item-menu-all-departments box-item-menu-"+index+"'> <div class='box-submenu'> <div class='box-submenu-1'></div> <div class='box-submenu-2'></div> </div> </div>");
				}else{
					$("#menu-vtex .menu-departamento").append("<div class='box-item-menu box-item-menu-"+index+"'> <div class='box-submenu'> <div class='box-submenu-1'></div> <div class='box-submenu-2'></div> </div> </div>");
				};
				
				if($(this).next("ul").find('li').length){
					$(this).next("ul").prependTo(".box-item-menu-"+index+" .box-submenu .box-submenu-1");
					$(this).addClass("drop-down");
				};
				
				$(this).prependTo(".box-item-menu-"+index);
			};

			if($(this).hasClass("all-departments")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/all-departments.png?a=1' width='14'>");
			}else if($(this).hasClass("informatica")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/informatic.png?a=1' width='31'>");
			}else if($(this).hasClass("automotivo")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/automotive.png?a=1' width='35'>");
			}else if($(this).hasClass("artigos-esportivos")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/sport-articles.png?a=1' width='20'>");
			}else if($(this).hasClass("alimentos-e-bebidas")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/food.png?a=1' width='23'>");
			}else if($(this).hasClass("cameras-de-seguranca")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/security.png?a=1' width='33'>");
			}else if($(this).hasClass("audio-video")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/tv.png?a=1' width='33'>");
			}else if($(this).hasClass("mobile")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/mobile.png?a=1' width='13'>");
			}else if($(this).hasClass("eletroportateis")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/eletroportateis.png?a=1' width='25'>");
			}else if($(this).hasClass("offers-week")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/offers-week.png?a=1' width='17'>");
			}else if($(this).hasClass("eletrodomesticos")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/icon-eletrodomesticos.png?a=1' width='20'>");
			}else if($(this).hasClass("acessorios-smartphones")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/smartphone.png?a=1' width='13'>");
			}else if($(this).hasClass("bolsas-acessorios")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/icon-mochilas.png?a=1' width='25'>");
			}else if($(this).hasClass("moda")){
				$(this).find("span").append("<img src='https://mercostore.vteximg.com.br/arquivos/moda.png?a=1' width='25'>");
			}
			
		});

		$("#menu-vtex .menu-departamento ul li.lvl-1-item").hover(function() {
			$("#menu-vtex .menu-departamento ul li ul.list.visible").removeClass("visible");
			$(this).find("ul.list").addClass("visible");
		});

		//Ajustes no menu mobile
		if($("#header-nav-mobile").length){
			var total = $('#header-nav-mobile .body .nav-items li.has-children').length;
			var category_text = "";
			$("#header-nav-mobile .body .nav-items li").each(function(){
				category_text = $(this).find("a.link-lvl-1").text();
				if(category_text.match(/Informática/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/informatic.png?a=1' width='32'></div>");
				}else if(category_text.match(/Automotivo/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/automotive.png?a=1' width='35'></div>");
				}else if(category_text.match(/Artigos Esportivos/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/sport-articles.png?a=1' width='20'></div>");
				}else if(category_text.match(/Alimentos e Bebidas/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/food.png?a=1' width='23'></div>");
				}else if(category_text.match(/Câmeras de Segurança/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/security_mobile.png?a=1' width='33'></div>");
				}else if(category_text.match(/Áudio Vídeo/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/tv.png?a=1' width='33'></div>");
				}else if(category_text.match(/Mobile/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/mobile.png?a=1' width='13'></div>");
				}else if(category_text.match(/Eletroportáteis/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/eletroportateis.png?a=1' width='25'></div>");
				}else if(category_text.match(/Eletrodomésticos/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/icon-eletrodomesticos.png?a=1' width='20'></div>");
				}else if(category_text.match(/Acessórios Smartphones/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/smartphone.png?a=1' width='13'></div>");
				}else if(category_text.match(/Bolsas Acessórios/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/icon-mochilas.png?a=1' width='25'></div>");
				}else if(category_text.match(/Moda/)){
					$(this).find("a.link-lvl-1").prepend("<div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/moda.png?a=1' width='25'></div>");
				}
			});
			
			$("#header-nav-mobile .body .nav-items li.has-children").each(function(index){
				if(index != total - 1){//Para não adicionar o icone no último item do menu, pois esse não possui link nem texto
					$(this).find("a.link-lvl-1").append("<i class='fas fa-chevron-right'></i>");
				}else{
					$(this).addClass("d-none");//Ocultando o último item do menu, pois esse não possui link nem texto
				};
			});
		};

		var offers_week_link = "";
		if($("a.offers-week-link").length){
			offers_week_link = $("a.offers-week-link").attr("href");
		};

		$("#header-nav-mobile .body .nav-items").append("<li class='lvl-1-item py-3'><a href='"+offers_week_link+"' class='link-lvl-1'><div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/ids/156339/offers-week.png' width='17'></div> Ofertas da Semana</a></li>");

		$("#header-nav-mobile .body .nav-items").prepend("<li class='lvl-1-item py-3 has-children'>"+
															"<a href='' class='link-lvl-1'><div class='box-img-menu-mobile'><img src='https://mercostore.vteximg.com.br/arquivos/ids/156332/all-departments.png' width='17'></div>Todos os Departamentos <i class='fas fa-chevron-right'></i></a>"+
															"<div class='nav-sub lvl-2'>"+
																"<div class='sub-header text-center'>"+
																	"<button class='btn-back btn'>"+
																		"<i class='fa fa-arrow-left' aria-hidden='true'></i>"+
																	"</button>"+
																	"<a class='link-lvl-1'>Todos os Departamentos</a>"+
																"</div>"+
																"<ul class='sub-categories sub-categories-all-categories'>"+
																"</ul>"+
															"</div>"+
														"</li>");

		if($(".sub-categories-all-categories").length){
			if($(".box-item-menu-all-departments .box-submenu .nav-items li.lvl-1-item a.link-lvl-1").length){
				$(".box-item-menu-all-departments .box-submenu .nav-items li.lvl-1-item a.link-lvl-1").each(function(){
					$(".sub-categories-all-categories").append("<li><a href='"+$(this).attr("href")+"'>"+$(this).html()+"</a></li>");
				});
			};
		};
	};
}