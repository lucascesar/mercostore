﻿import { getParameter, getSubString } from './_functions.js'

export default class PriceRange {
    constructor(element = '[data-admake="price-range"]') {
        this._element = element;
        this._$el = $(element);
        this._dataSelector = '';

        if (this._$el.length) {
            this._$el.attr('data-selector') ? this._dataSelector = this._$el.attr('data-selector') : this._dataSelector = element;
            this.init();
        }
    }
    createSlider(min, max, disabled = false) {
        noUiSlider.create(document.querySelector(this._dataSelector), {
            start: [min, max],
            connect: true,
            range: {
                'min': min,
                'max': max
            }
        });

        if ($('#department-navigator .search-multiple-navigator').is(':visible')) {
            $('#department-navigator .search-multiple-navigator').append(`<input id="input-range-price" rel="${encodeURI('fq=P:[' + min + '+TO+' + max + ']')}" class="multi-search-checkbox" type="checkbox" checked hidden>`);

            console.log(disabled);
            if (disabled) {
                $(this._dataSelector).attr('disabled', true);
            } else {
                $(this._dataSelector).removeAttr('disabled');
            }
        }
        this.events();
    }
    removeRange() {
        const $slider = document.querySelector(this._dataSelector),
            str = $('#department-showcase').html();

        let currentLocation = getSubString("buscapagina", "PageNumber", str), newUrl,
            rangeEncode = 'fq=P%3a%5b';

        if (currentLocation) {
            newUrl = currentLocation.substring(0, currentLocation.indexOf('&sl'));
            newUrl = newUrl.replace('buscapagina', 'busca');

            if (newUrl.indexOf(rangeEncode) > -1) {
                let param;

                param = '&' + getSubString(rangeEncode, '%5d', newUrl) + '%5d';
                newUrl = newUrl.replace(param, '');
            }
            return newUrl;
        }
        return false;
    }
    init() {
        const str = $('#department-showcase').html();
        let currentLocation = getSubString("buscapagina", "PageNumber", str),
            minValue, maxValue, rangeEncode = 'fq=P%3a%5b';

        if (currentLocation.indexOf(rangeEncode) > -1) {
            let param;

            $('#filterRemove').show();
            param = getSubString(rangeEncode, '%5d', currentLocation);

            if (param) {
                param = param.replace(rangeEncode, '').replace('%5d', '');
                minValue = parseFloat(param.split('+')[0]);
                maxValue = parseFloat(param.split('+')[2]);

                this.createSlider(minValue, maxValue, true);
            }
        } else {
            let order, url = currentLocation;

            $('#filterRemove').hide();
            if (url) {
                url = url.substring(0, url.indexOf('&sl'));
                url = url.replace('buscapagina', '/search');
                order = getParameter('O', url);

                if (order) {
                    url = url.replace('&O=' + order + '', '');
                } else {
                    url = url;
                }

                $.ajax({
                    "url": '/api/catalog_system/pub/products' + url + '&O=OrderByPriceDESC',
                    "method": "GET",
                    "headers": {
                        "Content-Type": "application/json",
                    },
                    success: (data) => {
                        if (data.length) {
                            minValue = 0;
                            maxValue = data[0].items[0].sellers[0].commertialOffer.Price;

                            this.createSlider(minValue, maxValue, false);
                        }
                    },
                    error: function (data, status) {
                        console.log('errors' + status);
                    },
                });
            }

        }
    }
    events() {
        let $slider = document.querySelector(this._dataSelector),
            $valueMin = document.getElementById('slider-margin-value-min'),
            $valueMax = document.getElementById('slider-margin-value-max');

        $slider.noUiSlider.on('update', function (values, handle) {
            if (handle) {
                $valueMax.innerHTML = values[handle];
            } else {
                $valueMin.innerHTML = values[handle];
            }
        });

        $slider.noUiSlider.on('end', function (values) {
            $('#department-navigator .search-multiple-navigator #input-range-price').attr('rel', `${encodeURI('fq=P:[' + values[0] + '+TO+' + values[1] + ']')}`)
            $('#input-range-price').click();
        });

        $(document).on('click', '#filterRanger', () => {
            let newUrl, sliderValues = $slider.noUiSlider.get();
            newUrl = this.removeRange();

            if (newUrl != '' && sliderValues.length) {
                newUrl = newUrl + `&fq=P%3a%5b${sliderValues[0]}+TO+${sliderValues[1]}%5d`;
                window.location = window.location.origin + '/' + newUrl;
            }
        });

        $(document).on('click', '#filterRemove', () => {
            let newUrl;
            newUrl = this.removeRange();

            if (newUrl != '') {
                window.location = window.location.origin + '/' + newUrl;
            }
        });

        $(document).on('click', '.multi-search-checkbox', function () {
            onSearchNavigatorCheckBoxClick(this);
        });
    }
}

