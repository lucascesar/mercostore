import getVariables from './_variables.js'
const _variables = getVariables('productImage');

function createMobile($element, $thumbs) {
    let htmlItems = '';

    if ($(window).width() < 992 && !$('.product-image-mobile').length) {

        $.each($thumbs.find('li a'), function () {
            var $this = $(this);
            var url = $this.attr('rel');

            htmlItems += '<li class="product-image-mobile-item"><img class="img-fluid" src="' + url + '" /></li>'
        });

        if (htmlItems !== '') {
            $element.find('#show').after(`<div class="product-image-mobile d-block d-lg-none">
                                                <ul id="product-image-mobile-carousel">
                                                    ${htmlItems}
                                                </ul>
                                            </div>`);
        }


        $('#product-image-mobile-carousel').slick({
            centerMode: false,
            dots: true,
            infinite: true,
            speed: 300,
            autoplay: true,
            slidesToShow: 1,
            adaptiveHeight: true,
            arrows: false,
        });
    }
};

function setThumbs($thumbs, config) {
    if ($thumbs.find('li').length > config.thumbsQty) {
        let thumbsConfig = {
            dots: false,
            infinite: true,
            slidesToShow: config.thumbsQty,
            prevArrow: config.prevArrow,
            nextArrow: config.nextArrow,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    }
                },
            ]
        }
        if (config.thumbVertical) {
            thumbsConfig['vertical'] = true;
            thumbsConfig['centerMode'] = false;
        }
        if (thumbsConfig !== null && $.fn.slick && $thumbs.length) {
            $thumbs.slick(thumbsConfig);
        }
    }
}

export default function productImage(element = '[data-admake="product-image"]') {

    if (element !== undefined && element !== null && $(element).length) {
        let $element = $(element);
        let $thumbs = $element.find('.thumbs');

        let config = { ..._variables };
        let dataConfig = $element.attr('data-config');

        if (dataConfig !== undefined && dataConfig !== null && dataConfig !== '') {
            dataConfig = dataConfig.replace(/\'/g, '"');
            config = { ..._variables, ...JSON.parse(dataConfig) };
        }

        if (config.mobile) {
            $(window).resize(function () {
                createMobile($element, $thumbs);
            });
            createMobile($element, $thumbs);
            $element.find('#show').addClass('d-none d-lg-block');
        }

        setThumbs($thumbs, config);

        let targetNode = document.querySelector('#include > #image');
        var callback = function () {
            if (config.mobile) {
                $('.product-image-mobile').remove();
                createMobile($element, $thumbs);
            }

            if ($thumbs.find('> li').length) {
                $thumbs.removeClass('slick-initialized slick-slider slick-vertical');
                setThumbs($thumbs, config);
            }
        };
        var observer = new MutationObserver(callback);
        observer.observe(targetNode, { childList: true });

    }
}