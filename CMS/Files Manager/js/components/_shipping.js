﻿import getVariables from './_variables.js'
import { checkSkuVariation, formatMoneyCheckout } from './_functions'
const _variables = getVariables();

export default class Shipping {
    constructor(element = '[data-admake="shipping"]') {
        this._element = element;
        this._$el = $(element);
        this._config = null;

        if (this._$el.length) {
            let dataConfig = this._$el.attr('data-config');

            if (dataConfig !== undefined && dataConfig !== null && dataConfig !== '') {
                dataConfig = dataConfig.replace(/\'/g, '"');
                this._config = { ..._variables.shipping, ...JSON.parse(dataConfig) };
            } else {
                this._config = { ..._variables.shipping };
            }
            this.createStruct();
            this.events();
        }
    }
    createStruct() {
        this._$el.append('<div id="shipping-results"></div>');
        let $dataSelector = $(this._config.inputText);

        if ($.fn.mask) {
            $dataSelector.mask("99999-999");
        }
    }
    calculeShipping(self) {
        const $cep = $(self._config.inputText);

        let $shippingResults = $('#shipping-results'),
            htmlDefault = $shippingResults.html();

        $shippingResults.html(self._config.loader);

        if (checkSkuVariation()) {
            if ($cep.val() == undefined || $cep.val() == null || $cep.val() == '') {
                $shippingResults.html('<p>CEP inválido.</p>');
                return false;
            }

            let href = $('#page-product a.buy-button').attr('href');
            let params = {};

            href.substr(href.indexOf('?') + 1, href.length).split("&").forEach(function (item) {
                params[item.split("=")[0]] = item.split("=")[1];
            });

            if (params.sku == undefined || params.sku == null) {
                $shippingResults.html('<p>SKU não encontrado</p>');
                return false;
            }

            let channel = skuJson_0.salesChannel, postalCode = $cep.val(),
                country = $('meta[name="country"]').attr('content'),
                items = [{
                    id: params.sku,
                    quantity: (params.qty || 1),
                    seller: (params.seller || 1),
                    salesChannel: (channel || 1)
                }];


            vtexjs.checkout.simulateShipping(items, postalCode, country)
                .done(function (result) {

                    let html = '';
                    let cep = result.postalCode;

                    if (result.messages !== undefined && result.messages !== null && result.messages.length) {
                        for (var x = 0; x < result.messages.length; x++) {
                            html += '	<div class="alert alert-' + result.messages[x].status + '">';
                            html += '		<p>' + result.messages[x].text + '</p>';
                            html += '	</div>';
                        }
                    }

                    let slas = (result.logisticsInfo[0].slas || []);

                    if (slas.length) {
                        html += '<table class="table-results table">';
                        html += '	<thead>';
                        html += '		<tr>';
                        html += '			<th class="value">' + self._config.titleValue; + '</th>';
                        html += '			<th class="text">' + self._config.titleMsg; + '</th>';
                        html += '		</tr>';
                        html += '	</thead>';
                        html += '	<tbody>';
                        for (var x = 0; x < slas.length; x++) {
                            html += '	<tr>';
                            html += '		<td class="value">R$ ' + formatMoneyCheckout(slas[x].price, 2, ',', '.') + '</th>';
                            html += '		<td class="text">' + self._config.resultInfo.replace(/{S-NAME}/g, slas[x].name).replace(/{S-CEP}/g, cep).replace(/{S-DAYS}/g, parseInt(slas[x].shippingEstimate) + ' dias') + '</th>';
                            html += '	</tr>';
                        }
                        html += '	</tbody>';
                        html += '</table>';
                    }

                    $shippingResults.html(html);
                });
        } else {
            $shippingResults.html('<p>Selecione um produto</p>');
        }
    }
    events() {
        $(document).on('click', this._config.btnCalc, (e) => {
            e.preventDefault();
            this.calculeShipping(this);
        });
    }
}


