﻿import { billetPrice } from './_functions.js'

export default function showcase(element = '.admake-showcase') {
    let _$el = $(element);

    if (_$el.length) {

        $('.ad-showcase-product').each(function () {
            let $flag = $(this).find('[class*="boleto"]'),
                $best = $(this).find('.best-price-bottom'),
                $discount = $(this).find('.percent.flag span'),
                $discountBF = $(this).find('.black-friday-discount > span:nth-child(1)');

            if ($discount.length) {
                let value = parseFloat($discount.html().replace(' %', '').replace(',', '.'));
                $discount.text(' ' + value.toFixed() + ' %');
            }
            if ($discountBF.length) {
                let value = parseFloat($discountBF.html().replace(' %', '').replace(',', '.'));
                $discountBF.text('-' + value.toFixed() + '%');
            }

            try {
                billetPrice($flag, $best, $best, 0, $(this).find('.best-price-bottom'));
            } catch (error) {
                
            }

            // $(this).find('.best-price-bottom').html(`R$ ${formatMoney(newBestPrice, 2, ',', '.')}<small>à vista</small>`);

        });

        // MUTATION
        if ($('body#page-department').length) {
            let targetNode = document.querySelector('#department-showcase .vitrine > .admake-showcase');
            var callback = function () {
                $('.ad-showcase-product').each(function () {
                    let $flag = $(this).find('[class*="boleto"]'),
                        $best = $(this).find('.best-price'),
                        $discount = $(this).find('.product-flags .percent.flag span'),
                        $discountBF = $(this).find('.black-friday-discount > span:nth-child(1)');

                    if ($discount.length) {
                        let value = parseFloat($(this).html().replace(' %', '').replace(',', '.'));
                        $discount.text(' ' + value.toFixed() + ' %');
                    }
                    if ($discountBF.length) {
                        let value = parseFloat($discountBF.html().replace(' %', '').replace(',', '.'));
                        $discountBF.text('-' + value.toFixed() + '%');
                    }

                    billetPrice($flag, $best, $best, 0);
                });
            };
            var observer = new MutationObserver(callback);
            observer.observe(targetNode, { childList: true });
        }

    }
}
