import getVariables from './_variables.js'
const _variables = getVariables();


export default class ShowcaseDepartment {
    constructor(element = '[data-admake=showcase-department]') {
        this._element = element;
        this._$el = $(element);
        this._config = null;

        if (this._$el.length) {
            let dataConfig = this._$el.attr('data-config');

            if (dataConfig !== undefined && dataConfig !== null && dataConfig !== '') {
                dataConfig = dataConfig.replace(/\'/g, '"');
                this._config = { ..._variables.showcaseDepartment, ...JSON.parse(dataConfig) };
            } else {
                this._config = { ..._variables.showcaseDepartment };
            }
            this.controler();
        }
    }
    createOrderButtons() {
        let htmlFilter = `<div id="order-buttons" class="${this._config.classesOrder}">
                            <span>Ordenar por</span>`;

        let $orderSelect = this._$el.find('.sub').eq(0).find('.orderBy > select');

        $.each($orderSelect.find('option'), function () {
            const $this = $(this);

            if ($this.attr('value')) {
                if ($orderSelect.val() == $this.attr('value')) {
                    htmlFilter += `<button class="btn-order btn selected" data-value="${$this.attr('value')}">${$this.text()}</button>`;
                } else {
                    htmlFilter += `<button class="btn-order btn" data-value="${$this.attr('value')}">${$this.text()}</button>`;
                }

            }
        });

        htmlFilter += `</div>`;
        this._$el.prepend(htmlFilter);

        /** event */
        $(document).on('click', '.btn-order', function () {
            let value = $(this).attr('data-value');
            $orderSelect.val(value).prop('selected', true);
            $orderSelect.change();
        });
    }
    createInfiniteScroll() {
        if ($.fn.QD_infinityScroll) {
            let configScroll = {
                lastShelf: ">div:last",
                elemLoading: `<div id="scrollLoading" class="qd-is-loading text-center">${this._config.loader}</div>`,
                searchUrl: null,
                returnToTop: $('<div id="returnToTop"><a href="#"><span class="arrowToTop"><i class="fas fa-arrow-up"></i></span></a></div>'),
                scrollBy: document
            }

            if (this._config.btnLoadMore) {
                if ($('.pager.bottom .page-number').length > 1) {
                    this._$el.append(`<div class="scroll-button text-center"><button id="handle-scroll" class="btn">${this._config.titleBtnLoad}</button></div>`);
                }

                configScroll.elemLoading = null;
                configScroll['callback'] = () => $('#handle-scroll').html(this._config.titleBtnLoad);
                configScroll['paginate'] = (scroll) => {

                    $(document).on('click', '#handle-scroll', () => {
                        $('#handle-scroll').html(this._config.loader);
                        if (!scroll()) {
                            $('#handle-scroll').hide();
                        }
                    });
                }
            } 
            $(this._config.showcase).QD_infinityScroll(configScroll);
            
        } else {
            console.log('plugin ScrollInfinito não instalado!!');
        }
    }
    controler() {
        if (this._config.orderButtons) {
            this.createOrderButtons();
        }
        if (this._config.infiniteScroll) {
            this.createInfiniteScroll();
        }
    }
}