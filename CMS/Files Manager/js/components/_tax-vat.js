﻿import { getCookies, setCookies } from './_functions.js'

export default function taxVat() {
    let $body = $('body');

    if (typeof(Storage) !== "undefined") {
        $('.btn-vat').removeClass('active');

        if(!getCookies('taxVat', 'section') || getCookies('taxVat', 'section') == "true"){
            $body.addClass('show-vat');
            $('.btn-show-tax').addClass('active');
        }else{
            $body.removeClass('show-vat');
            $('.btn-hide-tax').addClass('active');
        }
        $('.col-header-tax > button').html($('.btn-vat.active').html());
    }

    /** events */
    $(document).on('click', '.btn-show-tax', function () {
        $('.btn-vat').removeClass('active');
        $body.addClass('show-vat');
        $(this).addClass('active');
        setCookies('taxVat', true, 'section');

        $('.col-header-tax > button').html($('.btn-vat.active').html());
    });
    $(document).on('click', '.btn-hide-tax', function () {
        $('.btn-vat').removeClass('active');
        $body.removeClass('show-vat');
        $(this).addClass('active');
        setCookies('taxVat', false, 'section');

        $('.col-header-tax > button').html($('.btn-vat.active').html());
    });
    $(document).on('click', '#header-nav-mobile .nav-tax > button', function () {
        $(this).toggleClass('active');
    });
}
