﻿export default function titleContentExists(element = '[data-admake="title-content-exits"]') {
    
    $.each( $(element), function(){
        let $this = $(this);
        let $title = $($this.attr('data-title'), $this); 
        let $content = $($this.attr('data-content'), $this); 

        if( $title.length && $content.length ){
            if( $content.html() === undefined || $content.html() === null || $content.html() == '' ){
                $this.find('+ hr').remove();
                $this.remove();
            }
        }else{
            $this.find('+ hr').remove();
            $this.remove();
        }
    });
    
}