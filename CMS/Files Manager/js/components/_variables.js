const loader = '<i class="fas fa-sync fa-spin"></i>',
    prevArrow = `<button type="button" class="slick-prev btn"><i class="fas fa-chevron-left"></i></button>`,
    nextArrow = `<button type="button" class="slick-next btn"><i class="fas fa-chevron-right"></i></i></button>`;


const variables = {
    loader: loader,
    headers: {
        "Accept": "application/vnd.vtex.ds.v10+json",
        "Content-Type": "application/json",
        "x-vtex-api-appKey": "vtexappkey-mercostore-BRXSAS",
        "x-vtex-api-appToken": "TKGTYIZOAWPJDZCGUBPIKWXPAWUGRLCRRAPFYABNLCFQNOWRKXUQVKBXNRSBOTWOTXRCJXIUACWIQGBMZCWUQMVHYSRHIZZSFQWPWTHMOAPTNTGGPGCLWCYHCKERFBLB"
    },
    carousel: {
        dots: false,
        arrow: true,
        infinite: true,
        prevArrow: prevArrow,
        nextArrow: nextArrow
    },
    cart: {
        // structs
        elementMain: 'admake-cart-sidebar',
        title: 'Seu Carrinho',
        btnClose: '<i class="fas fa-times"></i>',
        loader: loader,
        selectorBadge: '.col-header-cart .badge',
        btnBuyMore: '',
        btnGoCart: 'Finalizar Carrinho',
        subTotal: false,
        total: true,
        discount: true,
        alets: [],
        // alerts: ['* Livrare calculată la încheierea comenzii.', '** Dacă aveți un tip de cupon în completarea comenzii.'],

        // items
        qtyItem: true,
        listPrice: false,
        bestPrice: true,
        btnRemove: '<i class="fas fa-trash"></i>',

        // buy
        buyAsyn: true,
        // elementBuy: '.buy-button-normal > a, #ad-product-buy-button .buy-button',
        elementBuy: '#ad-product-buy-button .buy-button',
        elementQty: '.btn-add-remove'
    },
    showcaseDepartment: {
        // scroll
        infiniteScroll: false,
        btnLoadMore: true,
        titleBtnLoad: 'Carregar mais produtos',
        showcase: '.admake-showcase[id*=ResultItems]',
        loader: loader,

        // filter
        orderButtons: false,
        classesOrder: 'd-xl-flex justify-content-between align-items-center text-center'
    },
    productImage: {
        carouselDesktop : false,
        mobile          : true,
        thumbsQty       : 5,
        thumbVertical   : false,
        prevArrow 		: prevArrow,
        nextArrow 		: nextArrow,
    },
    shipping: {
        loader: loader,
        calculateQty: true,
        titleValue: 'Valores',
        titleMsg: 'Disponibilidade',
        resultInfo: 'Frete {S-NAME}, entrega em {S-DAYS} para o CEP {S-CEP}',
        btnCalc: '#btn-shipping',
        inputText: '#cep-shipping'
    },
    reviews: {
        column: false,
        pagination: false,
        qtyByPage: 1
    },
    navbar: {
        // struct 
        levels: 3,
        fluid: false,
        subContainer: true,
        qtyNivel3: 5,
        showMore: 'ver mais',

        // all department - qtyMax (auto), orientation (vertical, horizontal)
        allDepartment: true,
        titleAll: "Todos os Departamentos",
        qtyMax: 0,
        orientation: 'vertical',

        // imgs - position (left, right)
        icon: false,
        bannerSub: false,
        positionBanner: 'right',

        //mobile - mobileType (dropdown, slider)
        mobile: true,
        mobileElement: '#header-nav-mobile .nav-menu ul',
        mobileBtnElement: '#header-menu button',
        mobileType: 'dropdown'
    },
    discountProgressive: {
        idCalculator: '718ad4e6-e2b1-4456-8775-6e457b7db377',
        textQty: 'Cantitate',
        textDiscount: 'Discount',
        btnBuy: '<i class="fas fa-cart-plus"></i>'
    },
    estimateDate: {
        title: 'LA CERERE',
        text: 'Produs disponibil la cerere. Livrarea produsului se va face in cel mult 30 de zile de la plasarea comenzii.'
    }
}

export default function getVariables(param) {
    return ( param !== undefined && param !== null && variables[param] !== undefined ) ? variables[param] : variables;
}