﻿// FUCTIONS
import { formatMoney } from './_functions.js'
import getVariables from './_variables.js'

const _variables = getVariables();

$(document).WishList({
    storeName: 'dacris',
    entityMasterData: 'WL',
    headers: _variables.headers,
    buttonAddProduct: '#btn-wishlist:not(.active)',
    atributeIdProduct: 'data-id',
    buttonRemoveProduct: '#btn-wishlist.active, .product-remove-wishlist',
    idPageWishlist: '#page-wishlist',
    productListPageWishlist: '#page-wishlist .admake-showcase ul',
    structShowcase: function (objProduct) {
        var sellers = objProduct.items[0].sellers[0];
        var img = objProduct.items[0].images[0].imageTag;

        img = img.replace('~', '');
        img = img.replace(/#width#/g, '240');
        img = img.replace(/#height#/g, '240');

        var html = `<li>
                        <div class="ad-showcase-product" data-id="${objProduct.productId}">
                        <button class="btn product-remove-wishlist" data-id="${objProduct.productId}"><i class="fas fa-trash-alt"></i></button>
                        <div class="product-image">
                            <a class="image" title="${objProduct.productName}" href="${objProduct.link}">${img}</a>
                            <a href="${objProduct.link}" class="product-details btn top" data-id="${objProduct.productId}">
                                <span class="text">Afla detalii</span>
                            </a>
                        </div>
                        <div class="product-title">
                            <a title="${objProduct.productName}" href="${objProduct.link}" class="ff-montserrat">${objProduct.productName}</a>
                        </div>
                        <div class="product-price">
                            <a title="${objProduct.productName}" href="${objProduct.link}">`;


        if (sellers.commertialOffer.ListPrice > sellers.commertialOffer.Price) {
            html += `<span class="old-price">${formatMoney(sellers.commertialOffer.ListPrice, 2, ',', '.')} lei</span>
                     <span class="best-price">${formatMoney(sellers.commertialOffer.Price, 2, ',', '.')} lei</span>`;
        } else {
            html += `<span class="best-price">${formatMoney(sellers.commertialOffer.Price, 2, ',', '.')} lei</span>`;
        }

        html += `<span class="best-price-tax d-none">${formatMoney((sellers.commertialOffer.Price + sellers.commertialOffer.Tax), 2, ',', '.')} lei</span>
                    </a>
                </div>
                <div class="product-discount-progressive" data-admake="discount-progressive" data-config="{'idCalculator': '718ad4e6-e2b1-4456-8775-6e457b7db377'}"
                    data-sku="${objProduct.items[0].itemId}">
                </div>
                <a href="${objProduct.link}" class="product-details btn bottom"
                    data-id="${objProduct.productId}" tabindex="0">
                    <span class="text">Afla detalii</span>
                </a>
            </div>
        </li>`;

        return html;
    },
    afterRemoveProductWishlistPagecallback: function ($el) {
        $el.closest('li').hide('slow/400/fast');
    },
    afterLoadWishlistPagecallback: function () {
        // $('.showcase ul').slick(_variables.carroselDefault);
    },
    emptyWishListPage: function () {
        $('.wishlist-body').append('<h2>Wishlist-ul este gol</h2>');
    }
});