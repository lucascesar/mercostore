import ShowcaseDepartment from '../components/_showcaseDepartment.js'
import PriceRange from '../components/_priceRange.js'

export default (element = '#page-department') => {
    if ($(element).length) {

        new PriceRange;
        new ShowcaseDepartment;

        /* filter*/
        $('#department-showcase .sub').prepend($('#department-showcase .resultado-busca-numero').eq(0));
        $('#department-navigator h4 + ul').each(function () {
            var $this = $(this);
            if (!$this.find('>li').length) {
                $this.remove();
            }
        });

        // search-multiple-navigator
        $('#department-navigator .search-multiple-navigator h4').remove();
        $('#department-navigator .search-single-navigator h4').each(function () {
            $('#department-navigator .search-multiple-navigator h3').after($(this).find('+ ul').clone());
            $('#department-navigator .search-multiple-navigator h3').after($(this).clone());
        });
        $('.col-navigator .department-range').after($('.col-navigator .search-filter-button'));

        $('#department-navigator .search-single-navigator h5, #department-navigator .search-multiple-navigator fieldset').eq(0).before('<hr>');
        $('.resultado-busca-filtro').prepend(`
            <button id="open-filter-mobile" class="d-block d-md-none btn btn-secondary btn-lg"><i class="fas fa-filter"></i>Filtrar</button>`);

        $('fieldset label input').after('<i></i>');
        /* filter*/


        if(!$('.department-description .content').length){
            $('.department-description').remove();
        }

        //events
        $(document).on('click', '#open-filter-mobile', function () {
            $(`#page-department .col-navigator`).fadeIn(100, function () {
                $(`.col-navigator .content`).animate({
                    left: 0
                }, 300);
            });
        });

        $(document).on('click', '.close-filter-mobile', function () {
            $(`.col-navigator .content`).animate({
                left: '-100%'
            }, 300, function () {
                $(`#page-department .col-navigator`).fadeOut(100);
            });
        });
    }
}