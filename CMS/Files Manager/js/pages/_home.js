﻿export default (element = '#page-home') => {
    if ($(element).length) {

        // counter
        var $counter = $('#counter'),
            userTimer = $('#user-timer').text(),
            currentDate = new Date(),
            finalDate = new Date(userTimer);

        if ($counter.length && (finalDate.getTime() >= currentDate.getTime())) {
            let $timer = $('#counter .timer');

            $counter.show();
            $counter.find('.slick-initialized').slick("refresh");

            $timer.countdown(userTimer, function (event) {
                $(this).html(
                    event.strftime(
                        `<ul>
                                <li class="days">
                                    %D
                                    <small>zi</small>
                                </li>
                                <li>
                                    %H
                                    <small>ore</small>
                                </li>
                                <li class="colon">:</li>
                                <li>
                                    %M
                                    <small>min</small>
                                </li>
                                <li class="colon">:</li>
                                <li>%S
                                    <small>sec</small>
                                </li>
                            </ul>`
                    )
                );
            })
            .on("finish.countdown", function () {
                $counter.hide().remove();
                $('#mini-banner-3').show();
                $('#mini-banner-3').slick("refresh");
            });
        } else {
            $counter.remove();
            $('#mini-banner-3').show();
            $('#mini-banner-3').slick("refresh");
        }
    }
}