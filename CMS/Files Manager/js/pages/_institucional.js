import Forms from '../components/_forms.js'

export default (element = '#page-institucional') => {
    if ($(element).length) {
        $('.institucional-nav a[href="' + window.location.pathname + '"]:first').addClass('active');
        new Forms;
    }
}