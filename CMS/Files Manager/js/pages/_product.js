import ProductImage from '../components/_product-image.js'
import Shipping from '../components/_shipping.js'
import Reviews from '../components/_reviews.js'
// import addRemQty from '../components/_addRemQty.js'
import titleContentExists from '../components/_titleContentExists.js'

import { billetPrice, formatMoney, convertStringMoneytoFloat } from '../components/_functions'

function showHideQty() {
    if ($('#ad-product-buy-button .buy-button').length && $('#ad-product-buy-button .buy-button').is(':visible')) {
        $('.quantity-selector-container').show();
        $('.product-seller').show();

        $('#ad-product-buy-button .buy-button').css('display', 'inline-block');
        $('.row-price-buy .portal-notify').hide();
    } else {
        $('.quantity-selector-container').hide();
        $('#ad-product-buy-button .buy-button').hide();
        $('.product-seller').hide();

        if ($('.notifyme.sku-notifyme').is(':visible')) {
            if (!$('.row-price-buy .portal-notify').length) {
                $('.row-price-buy').prepend($(`<div class="portal-notify col-12"></div>`));
                $('.row-price-buy .portal-notify').append($('.portal-notify-me-ref'));
            }

            $('.row-price-buy .portal-notify').show();
        }
    }
}

export default (element = '#page-product') => {

    if ($(element).length) {

        if( $('.product-main .black-friday-discount .get-pct').length ){
            if( $('.help-price .valor-de .skuListPrice').length ){
                let oldPrice = $('.help-price .valor-de .skuListPrice').text().replace('R$ ', '').replace('.', '').replace(',', '.');
                oldPrice = parseFloat(oldPrice);
                let bestPrice = $('.help-price .valor-por .skuBestPrice').text().replace('R$ ', '').replace('.', '').replace(',', '.');
                bestPrice = parseFloat(bestPrice);

                let pct = 100 - ( bestPrice / oldPrice ) * 100;
                $('.product-main .black-friday-discount .get-pct').html('-' + pct.toFixed(0) + '%');
                $('.black-friday-discount.percent').show();
            }
        }

        let $precoDe = $('.product-info-border .help-price .price-list-price .skuListPrice');
        if( $precoDe.length && !$('.product-info-border .product-page-black-friday-discount').length ){
            let percent = 0;
            let $precoPor = $('.product-info-border .help-price .price-best-price .skuBestPrice');
            let precoDe = convertStringMoneytoFloat($precoDe.text());
            let precoPor = convertStringMoneytoFloat($precoPor.text());
            
            percent = ( precoDe - precoPor ) / precoDe * 100;
            if( $('.product-image-flags .product-flags .flag.black-friday').length ){
                $('.product-image-flags .product-flags').after(
                    `<span class="product-page-black-friday-discount black-friday-discount percent">
                        <span>-${percent.toFixed()}%</span>
                        <span>BLACK FRIDAY</span>
                    </span>`
                );
            }
        }

        // addRemQty();
        titleContentExists();
        showHideQty();
        billetPrice();

        new ProductImage;
        new Shipping;
        new Reviews;

        $('#caracteristicas table').addClass('table table-striped');
        $('.buy-button.buy-button-ref').prepend('<img src="/arquivos/icon-cart-product.png"/>');

        if ($('.quantitySelector > label > input').length) {
            $('.quantitySelector > label').append(`
                    <div class="actions">
                        <button class="add-qty btn btn-add-remove" parent="#ad-product-buy-button" element=".quantitySelector > label > input" type="add" tabindex="0">+</button>
                        <button class="remove-qty btn btn-add-remove" parent="#ad-product-buy-button" element=".quantitySelector > label > input" type="remove" tabindex="0">-</button>
                    </div>`);
        }

        // events
        $(document).on('change', '.skuselector-specification-label', function () {
            showHideQty();
            billetPrice();
        });
    }
}

$(document).ready(function () {
    if($('.product-header .product-sku-brand .product-brand .brandName a').length){
        var brandLink = $('.product-header .product-sku-brand .product-brand .brandName a').attr('href');
        var arrBrandLink = brandLink.split('/');
        var newBrandLink = arrBrandLink[0]+"//"+arrBrandLink[2]+"/"+arrBrandLink[5];
        $('.product-header .product-sku-brand .product-brand .brandName a').attr('href',newBrandLink);
    }

    /*Tabs da descrição do produto na pαgina de produto*/
    if($('.box-description-product').length && $('.product-informations-tabs').length){
        var $title = $('.box-description-product .accordion-btn');
        var $content = $('.box-description-product');//.productDescription
        var index = 'description';
        
        $title.removeClass('accordion-btn');
        
        $('.product-informations-tabs .nav_tabs ul.box-content-tabs').append('<li class="box-content-'+index+' box-content-li">'+
            '<input type="radio" id="tab'+index+'" class="rd_tab" name="tabs" checked>'+
            '<label for="tab'+index+'" class="tab_label active"></label>'+
            '<div class="tab-content"></div>'+
        '</li>');
        
        $('.product-informations-tabs .nav_tabs ul.box-content-tabs li.box-content-'+index+' label').html($title);
        $('.product-informations-tabs .nav_tabs ul.box-content-tabs li.box-content-'+index+' .tab-content').html($content);
    
    };
    if($('.product-specification #caracteristicas').length && $('.product-informations-tabs').length){
        $('.product-specification #caracteristicas h4.group').each(function(index){
            var $title = $(this);
            var $content = $(this).next();
            
            $('.product-informations-tabs .nav_tabs ul.box-content-tabs').append('<li class="box-content-'+index+' box-content-li">'+
                                                                    '<input type="radio" id="tab'+index+'" class="rd_tab" name="tabs">'+
                                                                    '<label for="tab'+index+'" class="tab_label"></label>'+
                                                                    '<div class="tab-content"></div>'+
                                                                '</li>');
                                                                
            $('.product-informations-tabs .nav_tabs ul.box-content-tabs li.box-content-'+index+' label').html($title);
            $('.product-informations-tabs .nav_tabs ul.box-content-tabs li.box-content-'+index+' .tab-content').html($content);
        });
    };
    
    $('.product-informations-tabs .nav_tabs ul.box-content-tabs li').each(function(){
        $(this).find('label').appendTo('.header-product-informations-tabs .container');
    });
    
    $('.tab_label').click(function(){
        $('.tab_label').removeClass('active');
        $(this).addClass('active');
    });

    $('.nav_tabs .tab-content table').each(function(){
        $(this).addClass('table').addClass('table-striped');
    });
    /***************************************************/
});