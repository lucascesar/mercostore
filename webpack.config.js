const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
    mode: 'development',
    entry: './CMS/Files Manager/index.js',
    output: {
        path: path.join(__dirname, '/CMS/Files Manager/js/'),
        filename: 'admake-default.min.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            },
            {
                test: /\.(css|scss)$/,
                use: ExtractTextPlugin.extract({
                    use: ['css-loader', 'sass-loader']
                })
            },
            {
                test: /\.(png|jpg)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: path.join(__dirname, '/CMS/Files Manager/images/')
                    }
                }
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('../css/admake-styles.min.css')
    ]
}